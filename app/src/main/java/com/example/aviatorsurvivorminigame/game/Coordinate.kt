package com.example.aviatorsurvivorminigame.game

data class Coordinate(var x: Float, var y: Float)