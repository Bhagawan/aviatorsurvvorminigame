package com.example.aviatorsurvivorminigame.game

import android.annotation.SuppressLint
import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import androidx.appcompat.content.res.AppCompatResources
import androidx.core.content.res.ResourcesCompat
import androidx.core.graphics.drawable.toBitmap
import com.example.aviatorsurvivorminigame.R
import com.example.aviatorsurvivorminigame.util.Prefs
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlin.concurrent.fixedRateTimer
import kotlin.math.absoluteValue
import kotlin.random.Random

class GameView(context: Context, attributeSet: AttributeSet?) : View(context, attributeSet) {
    private var mWidth = 0
    private var mHeight = 0
    private var planeX = 0.0f
    private var planeY = 0.0f
    private var planeHeight = 1.0f
    private var planeWidth = 1.0f

    private var distance = 0
    private var life = 100

    private val planeBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.plane)
    private val explosionBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.explosion)
    private val explosionSprites = List<Bitmap>(6) { n -> Bitmap.createBitmap(explosionBitmap, n * explosionBitmap.width / 6, 0, explosionBitmap.width / 6, explosionBitmap.height)}
    private val missileBitmap = BitmapFactory.decodeResource(context.resources, R.drawable.bullet)
    private val restartBitmap = AppCompatResources.getDrawable(context,R.drawable.ic_baseline_replay)?.toBitmap()

    private var missileWidth = 1.0f
    private var missileHeight = 1.0f
    private var missileSpeed = 1.0f
    private var missileTimer = 0
    private val missileTimeInterval = 30
    private val missiles = ArrayList<Coordinate>()
    private val explosions = ArrayList<Explosion>()

    private var state = INTRO

    companion object {
        const val INTRO = 0
        const val FLYING = 1
        const val END = 2
    }

    init {
        fixedRateTimer("refresh", false, 0, 1000 / 60) {
            CoroutineScope(Dispatchers.Main).launch { invalidate() }
        }
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        if(w > 0) {
            mWidth = w - paddingStart - paddingEnd
            mHeight = h - paddingTop - paddingBottom
            planeY = mHeight / 2.0f
            planeHeight = mHeight / 10.0f
            planeWidth = planeHeight / planeBitmap.height * planeBitmap.width
            planeX = mWidth / 20.0f + planeWidth / 2
            missileHeight = planeHeight / 2
            missileWidth = missileHeight / missileBitmap.height * missileBitmap.width
            missileSpeed = mWidth / 180.0f
        }
    }

    override fun onDraw(canvas: Canvas?) {
        canvas?.let {
            if(state == FLYING) {
                updateMissiles()
                drawBounds(it)
                drawMissiles(it)
                drawPlane(it)
            }
            drawExplosions(it)
            drawUI(it)
        }
    }

    @SuppressLint("ClickableViewAccessibility")
    override fun onTouchEvent(event: MotionEvent?): Boolean {
        when(event?.actionMasked) {
            MotionEvent.ACTION_DOWN -> {
                when(state) {
                    FLYING -> setPlaneHeight(event.y)
                }
                return true
            }
            MotionEvent.ACTION_MOVE -> {
                when(state) {
                    FLYING -> setPlaneHeight(event.y)
                }
                return true
            }
            MotionEvent.ACTION_UP -> {
                when(state) {
                    FLYING -> setPlaneHeight(event.y)
                    INTRO -> restart()
                    END -> if(explosions.isEmpty()) restart()
                }
                return true
            }
        }
        return super.onTouchEvent(event)
    }

    /// Public

    fun getState() : AviatorGameState = AviatorGameState(planeX, planeY, missiles, explosions, missileTimer, state)

    fun setGameState(gameState: AviatorGameState) {
        planeX = gameState.planeX
        planeY = gameState.planeY
        missiles.clear()
        missiles.addAll(gameState.missiles)
        explosions.clear()
        explosions.addAll(gameState.explosions)
        missileTimer = gameState.missileTimer
        state = gameState.state
    }

    //// Private

    private fun drawPlane(c: Canvas) {
        val p = Paint()
        c.drawBitmap(planeBitmap, null, Rect((planeX - (planeWidth / 2)).toInt(), (planeY - planeHeight / 2).toInt(), (planeX + planeWidth / 2).toInt(), (planeY + planeHeight / 2).toInt()), p)
    }

    private fun drawBounds(c : Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.style = Paint.Style.FILL
        c.drawRect(0.0f, 0.0f, mWidth.toFloat(), 10.0f, p)
        c.drawRect(0.0f, mHeight - 10.0f, mWidth.toFloat(), mHeight.toFloat(), p)
    }

    private fun drawUI(c: Canvas) {
        val p = Paint()
        p.color = Color.WHITE
        p.strokeWidth = 3.0f
        p.textSize = 40.0f
        p.textAlign = Paint.Align.CENTER

        when(state) {
            INTRO -> {
                p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                c.drawRoundRect(mWidth * 0.15f, mHeight * 0.2f, mWidth * 0.85f, mHeight * 0.2f + 150, 20f, 20f, p)
                p.color = Color.WHITE
                c.drawText("Передвигайте самолет,", mWidth / 2.0f, mHeight * 0.2f + 50, p)
                c.drawText("чтобы уворачиваться от выстрелов", mWidth / 2.0f, mHeight * 0.2f + 100 , p)
                p.color = Color.BLACK
                c.drawRoundRect(mWidth * 0.13f, mHeight * 0.2f + 170, mWidth * 0.87f, mHeight * 0.2f + 230 + planeHeight, 20f, 20f, p)
                c.drawBitmap(planeBitmap, null, Rect((mWidth * 0.15f).toInt(), (mHeight * 0.2f + 200).toInt(), (mWidth * 0.15f + planeWidth).toInt(),  (mHeight * 0.2f + 200 + planeHeight).toInt()), p)
                c.drawBitmap(missileBitmap, null, Rect((mWidth * 0.85f - missileWidth).toInt(), (mHeight * 0.2f + 200 + (planeHeight - missileHeight) / 2).toInt(), (mWidth * 0.85f).toInt(),  (mHeight * 0.2f + 200 + (planeHeight + missileHeight) / 2).toInt()), p)
            }
            FLYING -> {
                val healthPadWidth = mWidth* 0.6f
                val healthLevel = (life / 100.0f) * healthPadWidth
                p.color = Color.BLUE
                c.drawRect(mWidth * 0.2f - 4, 0.0f, mWidth * 0.8f + 4, 28.0f, p)
                p.color = Color.GREEN
                c.drawRect(mWidth * 0.2f, 4.0f, mWidth * 0.2f + healthLevel, 24.0f, p)
                p.color = Color.RED
                c.drawRect(mWidth * 0.2f + healthLevel, 4.0f, mWidth * 0.8f, 24.0f, p)
                p.color = Color.WHITE
                p.textSize = 30.0f
                p.isFakeBoldText = true
                c.drawText((distance / 10.0f).toInt().toString(), mWidth / 2.0f, 50.0f, p)
            }
            END -> {
                if(explosions.isEmpty()) {
                    p.color = ResourcesCompat.getColor(context.resources, R.color.grey_dark, null)
                    c.drawRoundRect(mWidth * 0.15f, mHeight * 0.3f - 50, mWidth * 0.85f, mHeight * 0.3f + 100, 20f, 20f, p)
                    val pad = (restartBitmap?.width ?: 90) * 0.5f + 5
                    c.drawRoundRect(mWidth * 0.5f - pad, mHeight * 0.7f - pad, mWidth * 0.5f + pad, mHeight * 0.7f + pad, 20f, 20f, p)

                    p.color = Color.WHITE
                    c.drawText("Дистанция: ${distance / 10}", mWidth * 0.5f, mHeight * 0.3f, p)
                    p.color = Color.YELLOW
                    p.isFakeBoldText = true
                    c.drawText("Рекорд: ${Prefs.getRecord(context)}", mWidth * 0.5f, mHeight * 0.3f + 50, p)

                    p.style = Paint.Style.STROKE
                    p.color = Color.WHITE
                    c.drawRoundRect(mWidth * 0.5f - pad + 5, mHeight * 0.7f - pad + 5, mWidth * 0.5f + pad - 5, mHeight * 0.7f + pad - 5, 20f, 20f, p)

                    restartBitmap?.let { c.drawBitmap(it, (mWidth - it.width) * 0.5f, mHeight * 0.7f - it.height * 0.5f, p) }
                }
            }
        }
    }

    private fun drawMissiles(c : Canvas) {
        val p = Paint()
        for(missile in missiles) {
            c.drawBitmap(missileBitmap,null, Rect((missile.x - missileWidth / 2).toInt(),(missile.y - missileHeight / 2).toInt(),(missile.x + missileWidth / 2).toInt(), (missile.y + missileHeight / 2).toInt()), p)
        }
    }

    private fun drawExplosions(c : Canvas) {
        val p = Paint()
        var n = 0
        while(n < explosions.size) {
            explosions[n].time++
            if(explosions[n].time > explosions[n].duration) {
                explosions.removeAt(n)
                n--
            } else {
                val sN = (explosions[n].time / (explosions[n].duration / explosionSprites.size)).coerceAtMost(explosionSprites.size - 1)
                c.drawBitmap(explosionSprites[sN],null, Rect((explosions[n].x - missileHeight / 2).toInt(),(explosions[n].y - missileHeight / 2).toInt(),(explosions[n].x + missileHeight / 2).toInt(), (explosions[n].y + missileHeight / 2).toInt()), p)
            }
            n++
        }
    }

    private fun setPlaneHeight(y: Float) {
        planeY = y.coerceAtMost(mHeight - 10.0f).coerceAtLeast(planeHeight + 10.0f)
    }

    private fun updateMissiles() {
        distance++
        var n = 0
        while(n < missiles.size - 1) {
            missiles[n].x -= missileSpeed
            if((missiles[n].x - planeX).absoluteValue < ((planeWidth + missileWidth) / 2) && (missiles[n].y - planeY).absoluteValue < (planeHeight + missileHeight - 20) / 2 ) {
                explosions.add(Explosion(missiles[n].x - missileWidth / 2, missiles[n].y, 0, 60))
                missiles.removeAt(n)
                life -= 20
                if(life <= 0) {
                    if(Prefs.getRecord(context) < distance / 10) Prefs.saveRecord(context, distance / 10)
                    explosions.add(Explosion(planeX, planeY, 0, 60))
                    state = END
                }
                n--
            } else if(missiles[n].x < -missileWidth) {
                missiles.removeAt(n)
                n--
            }
            n++
        }
        if(missileTimer++ > missileTimeInterval) {
            missileTimer = 0
            spawnMissile()
        }
    }

    private fun spawnMissile() {
        var y = 0
         while(y <= 0) {
            val r = Random.nextInt((missileHeight / 2).toInt(), (mHeight - 10 - missileHeight / 2).toInt())
            for(m in missiles) {
                if(m.x > mWidth - missileWidth && (m.y - r).absoluteValue < missileHeight) continue
            }
             y = r
        }
        missiles.add(Coordinate(mWidth.toFloat() + missileWidth / 2, y.toFloat()))
    }

    private fun restart() {
        state = FLYING
        planeY = mHeight / 2.0f
        distance = 0
        life = 100
    }
}