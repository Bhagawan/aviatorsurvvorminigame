package com.example.aviatorsurvivorminigame.game

import androidx.lifecycle.ViewModel

class GameViewModel: ViewModel() {
    private var gameState: AviatorGameState? = null

    fun saveGameState(state: AviatorGameState) {
        gameState = state
    }

    fun getGameState() : AviatorGameState? = gameState
}