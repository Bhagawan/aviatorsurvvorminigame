package com.example.aviatorsurvivorminigame.game

data class AviatorGameState(val planeX: Float,
                            val planeY: Float,
                            val missiles: List<Coordinate>,
                            val explosions: List<Explosion>,
                            val missileTimer: Int,
                            val state: Int)
