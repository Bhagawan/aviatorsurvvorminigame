package com.example.aviatorsurvivorminigame.game

data class Explosion(val x: Float, val y: Float, var time: Int, val duration: Int)
