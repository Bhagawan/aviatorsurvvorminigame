package com.example.aviatorsurvivorminigame

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.lifecycle.ViewModelProvider
import com.example.aviatorsurvivorminigame.databinding.ActivityMainBinding
import com.example.aviatorsurvivorminigame.game.GameViewModel

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    private lateinit var gameViewModel : GameViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        gameViewModel = ViewModelProvider(this)[GameViewModel::class.java]

        setContentView(binding.root)
    }

    override fun onResume() {
        val gameState = gameViewModel.getGameState()
        gameState?.let {
            binding.game.setGameState(it)
        }
        super.onResume()
    }

    override fun onPause() {
        gameViewModel.saveGameState(binding.game.getState())
        super.onPause()
    }
}