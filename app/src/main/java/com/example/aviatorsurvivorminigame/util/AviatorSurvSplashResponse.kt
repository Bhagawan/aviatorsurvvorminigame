package com.example.aviatorsurvivorminigame.util

import androidx.annotation.Keep

@Keep
data class AviatorSurvSplashResponse(val url : String)