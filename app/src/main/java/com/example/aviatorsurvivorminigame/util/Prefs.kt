package com.example.aviatorsurvivorminigame.util

import android.content.Context
import java.util.*

class Prefs {
    companion object {
        fun saveRecord(context: Context, record: Int) {
            val shP = context.getSharedPreferences("AviatorGame", Context.MODE_PRIVATE)
            shP.edit().putInt("record", record).apply()
        }

        fun getRecord(context: Context): Int = context.getSharedPreferences("AviatorGame", Context.MODE_PRIVATE).getInt("record", 0)


        fun getId(context: Context) : String {
            val shP = context.getSharedPreferences("AppData", Context.MODE_PRIVATE)
            var id = shP.getString("id", "default") ?: "default"
            if(id == "default") {
                id = UUID.randomUUID().toString()
                shP.edit().putString("id", id).apply()
            }
            return id
        }
    }
}